package uy.com.antel;


import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public class ProcesarMatriz {

    private final static String WORK_DIR = "C:\\Tesina\\";
    private final static String MATRIZ_DIR = "Matriz_OD\\";

    //private static int[][] matriz = new int[5604][5604];
    private static int[][] matriz = new int[6150][6150];

    public static void main(String[] args) throws Exception {

        procesarMatriz();
        //guardarUbicacionesProcesadas(leerUbicacionesCrudas());
        //paradasToImg(leerUbicacionesProcesadas());
    }

    private static List<String> leerUbicacionesCrudas() throws Exception {
        Map<Integer, String> cache = new HashMap<>();

        try (BufferedReader br = new BufferedReader(new FileReader(WORK_DIR + "ubicacionCruda.txt"))) {
            for (String line; (line = br.readLine()) != null; ) {
                final String[] split = line.split("\t");
                cache.putIfAbsent(Integer.valueOf(split[0]), split[1] + "\t" + split[2]);
            }
        } catch (FileNotFoundException fnfe) {
            System.err.println(fnfe.getMessage());
        }

        List<Map.Entry<Integer, String>> elementos = new ArrayList<>(cache.entrySet());
        elementos.sort(Comparator.comparing(Map.Entry::getKey));
        return elementos.stream().map(v -> v.getKey() + "\t" + v.getValue()).collect(Collectors.toList());
    }

    private static List<String> leerUbicacionesProcesadas() throws Exception {
        List<String> elementos = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(WORK_DIR + "ubicacionProcesada.txt"))) {
            for (String line; (line = br.readLine()) != null; ) {
                elementos.add(line);
            }
        } catch (FileNotFoundException fnfe) {
            System.err.println(fnfe.getMessage());
        }

        return elementos;
    }

    private static void guardarUbicacionesProcesadas(List<String> ubicaciones) throws Exception {
        try (PrintWriter writer = new PrintWriter(WORK_DIR + "ubicacionProcesada.txt", "UTF-8")) {
            for (String elemento : ubicaciones) {
                writer.println(elemento.replace(',', '.'));
            }
        }
    }

    private static void paradasToImg(List<String> paradas) throws Exception {

        final Double xProp = 30.8175195144;
        final Double yProp = 25.2991694915;

        final Double xShift = 554395.5453556540;
        final Double yShift = 6134610.9221502500;

        final BufferedImage res = new BufferedImage(1210, 927, BufferedImage.TYPE_INT_RGB);
        for (String elemento : paradas) {
            final String[] split = elemento.split("\t");
            final Double xImg = (Double.valueOf(split[1]) - xShift) / xProp;
            final Double yImg = (Double.valueOf(split[2]) - yShift) / yProp;
            try {
                res.setRGB(xImg.intValue(), 926 - yImg.intValue(), Color.red.getRGB());
            } catch (ArrayIndexOutOfBoundsException aiobe) {
                System.err.println("Valores: (" + xImg.intValue() + " - " + yImg.intValue() + ")");
            }
        }
        ImageIO.write(res, "bmp", new File(WORK_DIR + "paradas.bmp"));
    }

    private static void procesarMatriz() throws Exception {
        for (int i = 5; i <= 12; i++) {
            final String month = formatName(i);
            final String filePath = WORK_DIR + MATRIZ_DIR + month;

            int acumulado = 0;
            System.out.println("Procesando: " + filePath);
            for (int j = 546; j <= 6150; j++) {
                try (BufferedReader br = new BufferedReader(new FileReader(filePath + "\\" + j))) {
                    int k = 0;
                    for (String line; (line = br.readLine()) != null; ) {
                        k++;
                        acumulado += Integer.valueOf(line);
                        matriz[j - 1][k - 1] = Integer.valueOf(line);
                    }
                } catch (FileNotFoundException fnfe) {
                    System.err.println(fnfe.getMessage());
                }
            }
            System.out.println("Acumulado: " + acumulado);

            //toCSV(month);
            toBMP(month);
            break;
        }
        System.out.println("Finish");
    }

    private static void toBMP(String name) throws Exception {

        final BufferedImage res = new BufferedImage(6150, 6150, BufferedImage.TYPE_INT_RGB);
        for (int j = 0; j < 6150; j++) {
            for (int k = 0; k < 6150; k++) {
                if (matriz[j][k] == 0) {
                    res.setRGB(j, k, 0);
                    continue;
                }
                if (matriz[j][k] > 0) {
                    res.setRGB(j, k, Color.red.getRGB() - 1000 * matriz[j][k]);
                    continue;
                }
                if (matriz[j][k] > 50) {
                    res.setRGB(j, k, Color.red.getRGB());
                    continue;
                }
                if (matriz[j][k] > 25) {
                    res.setRGB(j, k, Color.orange.getRGB());
                    continue;
                }
                if (matriz[j][k] > 10) {
                    res.setRGB(j, k, Color.yellow.getRGB());
                    continue;
                }

                res.setRGB(j, k, Color.green.getRGB());
            }
        }

        ImageIO.write(res, "bmp", new File(WORK_DIR + MATRIZ_DIR + name + ".bmp"));
    }

    private static void toCSV(String name) throws Exception {
        try (PrintWriter writer = new PrintWriter(WORK_DIR + name + ".csv", "UTF-8")) {
            for (int j = 0; j < 6150; j++) {
                writer.print(matriz[j][0]);
                for (int k = 1; k < 6150; k++) {
                    writer.print("; " + matriz[j][k]);
                }
                writer.println();
            }
        }
    }

    private static String formatName(int i) {
        return i < 10 ? "0" + i : "" + i;
    }
}
