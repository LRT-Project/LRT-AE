viajes=3100645;

propBus=0.25;
propAutos=0.32;

propHoraPico=0.07725;

m_og=dlmread ('10.csv',';',0,0);

sumaDemanda=sum(sum(m_og));

s1=(m_og*viajes*propBus*propHoraPico)/sumaDemanda;
s2=(m_og*viajes*(propBus+propAutos)*propHoraPico)/sumaDemanda;


s1 = sparse(ceil(s1));
s2 = sparse(ceil(s2));

save '10-bus.mat' s1 ;
save '10-auto.mat' s2 ;
clear m_og;