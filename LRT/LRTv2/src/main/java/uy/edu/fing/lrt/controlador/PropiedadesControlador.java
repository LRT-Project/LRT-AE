package uy.edu.fing.lrt.controlador;

import uy.edu.fing.lrt.util.Logger;
import uy.edu.fing.lrt.util.PropiedadesEnum;

import java.awt.*;
import java.io.*;
import java.util.Arrays;
import java.util.Properties;

public final class PropiedadesControlador {

    private static Properties p = new Properties();
    private static final String SPLIT_SEPARATOR = ";";

    private PropiedadesControlador() {
    }

    static {
        try {
            load(new File("properties/instancia1.prop"));
        } catch (Exception ignore) {
        }
    }

    public static void setProperty(PropiedadesEnum key, String value) {
        p.put(key.getNombre(), value);
    }

    public static String getProperty(PropiedadesEnum prop) {
        getProps();
        String value = (String) p.get(prop.getNombre());
        return value == null ? prop.getValorDefecto() : value;
    }

    public static String[] getProperties(PropiedadesEnum prop) {
        String propArr = getProperty(prop);
        if (propArr == null || "".equals(propArr)) {
            return null;
        }
        return propArr.split(SPLIT_SEPARATOR);
    }

    public static Color[] getColorsProperties(PropiedadesEnum prop) {
        String[] propArr = getProperties(prop);

        if (propArr == null) {
            return null;
        }

        return Arrays.stream(propArr).map(s -> {
            int r = Integer.parseInt(s.substring(0, 2), 16);
            int g = Integer.parseInt(s.substring(2, 4), 16);
            int b = Integer.parseInt(s.substring(4, 6), 16);
            return new Color(r, g, b);
        }).toArray(Color[]::new);

    }

    public static Boolean getBoolProperty(PropiedadesEnum prop) {
        getProps();
        String propI = getProperty(prop);
        if (propI == null || "".equals(propI)) {
            return Boolean.FALSE;
        }
        return Boolean.valueOf(propI);
    }

    public static Integer getIntProperty(PropiedadesEnum prop) {
        getProps();
        String propI = getProperty(prop);
        if (propI == null || "".equals(propI)) {
            return null;
        }
        return Integer.valueOf(propI);
    }

    public static Double getDoubleProperty(PropiedadesEnum prop) {
        getProps();
        String propI = getProperty(prop);
        if (propI == null || "".equals(propI)) {
            return null;
        }
        return Double.valueOf(propI);
    }

    public static void saveTest() {
        getProps();
        try (FileOutputStream fos = new FileOutputStream("mor.properties")) {
            p.store(fos, null);
        } catch (IOException ex) {
            Logger.error(null, ex);
        }
    }

    public static void load(InputStream is) {
        try {
            p = new Properties();
            p.load(is);
        } catch (IOException ex) {
            throw new RuntimeException("Error leyendo el archivo de propiedades", ex);
        }
    }

    public static void load(File propFile) {
        try (FileInputStream is = new FileInputStream(propFile)) {
            load(is);
        } catch (IOException ex) {
            throw new RuntimeException("Error leyendo el archivo de propiedades", ex);
        }
    }

    private static Properties getProps() {
        if (p == null) {
            p = new Properties();
            Arrays.asList(PropiedadesEnum.values()).forEach((value) -> {
                p.put(value.getNombre(), value.getValorDefecto());
            });
        }
        return p;
    }

    public static void printValues() {
        Logger.info("-----------------------   VALORES   -------------------------");
        for (PropiedadesEnum prop : PropiedadesEnum.values()) {
            Logger.info(prop.getNombre() + "\t" + getProperty(prop));
        }
    }
}