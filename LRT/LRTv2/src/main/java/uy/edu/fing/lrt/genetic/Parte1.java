package uy.edu.fing.lrt.genetic;

import java.util.Arrays;
import java.util.List;
import uy.edu.fing.lrt.controlador.ProblemaControlador;
import uy.edu.fing.lrt.controlador.PropiedadesControlador;
import uy.edu.fing.lrt.modelo.Arista;
import uy.edu.fing.lrt.util.GlpkUtil;
import uy.edu.fing.lrt.util.PropiedadesEnum;

public final class Parte1 {

    //Clase metodos estaticos
    private Parte1() {
    }

    public static synchronized List<Arista> run() {

        ProblemaControlador problema = ProblemaControlador.getInstance();

        List<Arista> aristas = problema.getAristas();
        String[] especialesStr = PropiedadesControlador.getProperty(PropiedadesEnum.ESPECIALES).split(";");
        Integer[] especiales = Arrays.stream(especialesStr).map(Integer::valueOf).toArray(Integer[]::new);

        return GlpkUtil.run(aristas, especiales);
    }
}
