package uy.edu.fing.lrt.modelo;

import uy.edu.fing.lrt.controlador.PropiedadesControlador;
import uy.edu.fing.lrt.util.MathUtil;
import uy.edu.fing.lrt.util.PropiedadesEnum;

public final class Arista {

    private int id;

    private final int idNodoA;
    private final int idNodoB;

    private Nodo nodoA;
    private Nodo nodoB;

    private final int costo;
    private final int largo;
    private final int delay;

    public Arista(String line) {

        final Integer tiempo = PropiedadesControlador.getIntProperty(PropiedadesEnum.ESTACION_TIEMPO_INTERCAMBIO);
        final Integer velocidad = PropiedadesControlador.getIntProperty(PropiedadesEnum.BUS_VELOCIDAD);
        final Double aceleracion = PropiedadesControlador.getDoubleProperty(PropiedadesEnum.BUS_ACELERACION);

        String[] split = line.split("\t");
        boolean tieneLargo = split.length == 5;


        this.idNodoA = Integer.valueOf(split[0]);
        this.idNodoB = Integer.valueOf(split[1]);

        this.costo = Integer.valueOf(split[2]);

        if (tieneLargo) {
            this.largo = Integer.valueOf(split[3]);
            this.delay = tiempo + MathUtil.calcularTiempoPorDistancia(velocidad, aceleracion, this.largo);
        } else {
            this.largo = 0;
            this.delay = Integer.valueOf(split[3]);
        }
    }

    public int getCosto() {
        return costo;
    }

    public int getLargo() {
        return largo;
    }

    public int getDelay() {
        return delay;
    }

    public static int costo(Arista a) {
        return a.costo;
    }

    public static int largo(Arista a) {
        return a.largo;
    }

    public static int delay(Arista a) {
        return a.delay;
    }

    public int getIdNodoA() {
        return idNodoA;
    }

    public int getIdNodoB() {
        return idNodoB;
    }

    public Nodo getNodoA() {
        return nodoA;
    }

    public Nodo getNodoB() {
        return nodoB;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNodes(Nodo a, Nodo b) {
        nodoA = a;
        nodoB = b;
    }

    @Override
    public String toString() {
        return "Arista{"
                + "id=" + id
                + ", idNodoA=" + idNodoA
                + ", idNodoB=" + idNodoB
                + ", costo=" + costo
                + ", largo=" + largo
                + ", delay=" + delay + '}';
    }

}
