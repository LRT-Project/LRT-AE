package uy.edu.fing.lrt.util;

import uy.edu.fing.lrt.modelo.Nodo;
import uy.edu.fing.lrt.test.Parada;

public final class MathUtil {

    //Clase con metodos estaticos
    private MathUtil() {
    }

    /**
     * Velocidad Final en KM/H
     * Aceleracion en m/ss
     * <p>
     * este metodo es propio al porblema, se necesita el doble del resultado esperado
     *
     * @param velFinal
     * @param aceleracion
     * @return
     */
    public static Double calcularDistanciaParaAlcanzarVelocidad(int velFinal, Double aceleracion) {
        double v = ((double) velFinal) / 3.6;
        return v * v / (aceleracion);
    }

    public static Double calcularTiempoParaAlcanzarVelocidad(int velFinal, Double aceleracion) {
        double v = ((double) velFinal) / 3.6;
        return 2 * v / (aceleracion);
    }

    public static int calcularTiempoPorDistancia(int velFinal, Double aceleracion, int distancia) {
        double v = ((double) velFinal) / 3.6;
        double tiempoAcelerando = v / (aceleracion);
        double tiempoAyF = 2 * tiempoAcelerando;
        double distanciaAyF = v * tiempoAcelerando;

        Double result;
        if (distancia < distanciaAyF) {
            result = 2 * Math.sqrt(distancia / aceleracion);
        } else {
            result = tiempoAyF + (distancia - distanciaAyF) / v;
        }
        return result.intValue();
    }

    public static Double dist(Nodo n1, Parada n2) {
        final double distX = n1.getCoordX() - n2.getCoordX();
        final double distY = n1.getCoordY() - n2.getCoordY();

        return Math.sqrt(distX * distX + distY * distY);
    }

    public static Double plus(Double a, Double b) {
        return a * b;
    }

    public static Double plus(Double a, Integer b) {
        return a * Double.valueOf(b);
    }


}
