package uy.edu.fing.lrt.modelo;

import uy.edu.fing.lrt.controlador.ProblemaControlador;
import uy.edu.fing.lrt.controlador.PropiedadesControlador;
import uy.edu.fing.lrt.util.MathUtil;
import uy.edu.fing.lrt.util.PropiedadesEnum;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class Individuo {

    public Long costoConstruccion = null;
    public Long costoOperacion = null;
    private Long costoUsuario = null;
    private String id = null;
    private Concentrador[] concentradores;

    public Individuo(Individuo copy) {
        this.costoConstruccion = copy.costoConstruccion;
        this.costoOperacion = copy.costoOperacion;
        this.costoUsuario = copy.costoUsuario;
        this.concentradores = new Concentrador[copy.concentradores.length];
        IntStream.range(0, concentradores.length).forEach(i -> this.concentradores[i] = new Concentrador(copy.concentradores[i]));
    }

    public Individuo(Concentrador[] concentradores) {
        this.concentradores = concentradores;
    }

    public Long getCosto() {
        if (costoUsuario == null) {
            calcularCostoUsuario();
        }

        return getCostoDinero() + costoUsuario;
    }

    public Long getCostoDinero() {
        if (costoConstruccion == null) {
            calcularCostoConstruccion();
        }
        if (costoOperacion == null) {
            calcularCostoOperacion();
        }
        return costoConstruccion + costoOperacion;
    }

    private void checkOperacion() {
        boolean recalcularFrec = getLineas().stream().anyMatch(e -> e.getFrecuencia() == null);
        if (recalcularFrec) {
            ProblemaControlador.getInstance().calculoDeFrecuencias(this);
        }
    }

    private void calcularCostoConstruccion() {
        Double factorA = PropiedadesControlador.getDoubleProperty(PropiedadesEnum.FITNESS_A);
        costoConstruccion = MathUtil.plus(factorA, getAristas().stream().map(Arista::getCosto).reduce(0, Integer::sum)).longValue();
    }

    private void calcularCostoOperacion() {
        Double factorB = PropiedadesControlador.getDoubleProperty(PropiedadesEnum.FITNESS_B);
        checkOperacion();
        costoOperacion = MathUtil.plus(factorB, getLineas().stream().map(l -> l.getLargo() * l.getFrecuencia()).reduce(0, Integer::sum)).longValue();
    }

    private void calcularCostoUsuario() {
        Double factorC = PropiedadesControlador.getDoubleProperty(PropiedadesEnum.FITNESS_C);
        checkOperacion();
        costoUsuario = MathUtil.plus(factorC, ProblemaControlador.getInstance().calculoDeCostoUsuario(this)).longValue();
    }

    public boolean isValido() {
        return Arrays.stream(concentradores).allMatch(Concentrador::isValido);
    }

    public List<Nodo> getNodos() {
        Map<Integer, Nodo> tmp = new HashMap<>();
        Arrays.stream(concentradores)
                .forEachOrdered(concentrador -> concentrador.getLineas()
                        .forEach(linea -> linea.getTramos()
                                .forEach(arista -> {
                                    tmp.put(arista.getIdNodoA(), arista.getNodoA());
                                    tmp.put(arista.getIdNodoB(), arista.getNodoB());
                                })));
        return new ArrayList<>(tmp.values());
    }

    public Linea getLinea(int l) {
        int lineaId = 0;
        for (Concentrador concentrador : concentradores) {
            for (Linea linea : concentrador.getLineas()) {
                if (lineaId == l) {
                    return linea;
                }
                lineaId++;
            }
        }
        return null;
    }

    public List<Linea> getLineas() {
        return Arrays.stream(concentradores).flatMap(e -> e.getLineas().stream()).collect(Collectors.toList());
    }

    public List<Arista> getAristas() {
        Map<Integer, Arista> tmp = new HashMap<>();
        for (Concentrador concentrador : concentradores) {
            for (Linea linea : concentrador.getLineas()) {
                for (Arista arista : linea.getTramos()) {
                    tmp.put(arista.getId(), arista);
                }

            }
        }
        return new ArrayList<>(tmp.values());
    }

    public Concentrador[] getConcentradores() {
        return concentradores;
    }

    private void genId() {
        id = getAristas().stream().map(Arista::getId).sorted().map(e -> e + "").collect(Collectors.joining("-"));
    }

    public String getId() {
        if (id == null) {
            genId();
        }
        return id;
    }

    public boolean igualA(Individuo other) {
        boolean encontre;
        for (Concentrador c1 : concentradores) {
            encontre = false;
            for (Concentrador c2 : other.getConcentradores()) {
                if (c1.igualA(c2)) {
                    encontre = true;
                    break;
                }
            }
            if (!encontre) {
                return false;
            }
        }
        return true;
    }
}
