package uy.edu.fing.lrt.genetic;

import uy.edu.fing.lrt.controlador.ProblemaControlador;
import uy.edu.fing.lrt.controlador.PropiedadesControlador;
import uy.edu.fing.lrt.modelo.Concentrador;
import uy.edu.fing.lrt.modelo.Individuo;
import uy.edu.fing.lrt.util.PropiedadesEnum;
import uy.edu.fing.lrt.util.Random;

import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

public final class Cruzador {

    //Clase con metodos estaticos
    private Cruzador() {
    }

    public static List<Individuo> run(List<Individuo> candidatos) {
        List<Individuo> result;
        Integer algoritmo = PropiedadesControlador.getIntProperty(PropiedadesEnum.CRUZAMIENTO);
        switch (algoritmo) {
            case 0:
                result = candidatos;
                break;
            case 1:
                result = algoritmo1(candidatos);
                break;
            default:
                throw new RuntimeException("No se ha definido una acion " + algoritmo + " para la propiedad " + PropiedadesEnum.CRUZAMIENTO.getNombre());
        }
        result.forEach(e -> ProblemaControlador.getInstance().calculoDeFrecuencias(e));
        return result;
    }

    /* Algoritmo que realiza un cruzamiento seleccionando con una probabilidad x 
    sla cantdad de líneas que tome de un padre o de la madre en el cruzamiento*/
    private static List<Individuo> algoritmo1(List<Individuo> candidatos) {
        Integer cantidadCruzamientos = PropiedadesControlador.getIntProperty(PropiedadesEnum.CRUZAMIENTO_CANTIDAD);

        List<Individuo> resultado = Collections.synchronizedList(candidatos);

        IntStream.range(0, cantidadCruzamientos).forEach(j -> {

            int p1 = Random.get(candidatos.size());
            int p2 = Random.get(candidatos.size());

            Individuo padre1 = candidatos.get(p1);
            Individuo padre2 = candidatos.get(p2);

            Individuo[] hijos = cruzar(padre1, padre2);

            resultado.add(hijos[0]);
            resultado.add(hijos[1]);
        });
        return resultado;
    }

    private static Individuo[] cruzar(Individuo padre1, Individuo padre2) {
        Integer cantFuentes = PropiedadesControlador.getIntProperty(PropiedadesEnum.CANT_FUENTES);
        Integer probCruz = PropiedadesControlador.getIntProperty(PropiedadesEnum.CRUZAMIENTO_PROBABILIDAD);

        Concentrador[] concentradoresP1 = padre1.getConcentradores();
        Concentrador[] concentradoresP2 = padre2.getConcentradores();

        Concentrador[] concentradoresH1 = new Concentrador[cantFuentes];
        Concentrador[] concentradoresH2 = new Concentrador[cantFuentes];

        IntStream.range(0, cantFuentes).parallel().forEach(i -> {
            if (Random.prob(probCruz)) {
                concentradoresH1[i] = concentradoresP1[i];
                concentradoresH2[i] = concentradoresP2[i];
            } else {
                concentradoresH1[i] = concentradoresP2[i];
                concentradoresH2[i] = concentradoresP1[i];
            }
        });

        Individuo hijo1 = new Individuo(concentradoresH1);
        Individuo hijo2 = new Individuo(concentradoresH2);

        return new Individuo[]{hijo1, hijo2};
    }

}
