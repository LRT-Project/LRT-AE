package uy.edu.fing.lrt.modelo;

import java.util.*;
import java.util.stream.Collectors;

public final class Concentrador {

    private Integer cantLineas;
    private List<Linea> lineas;
    private String id = null;
    private Boolean flip=false;

    public Concentrador(Concentrador copy) {
        cantLineas = copy.cantLineas;
        id = null;
        lineas = new ArrayList<>();
        copy.lineas.forEach(linea -> lineas.add(new Linea(linea)));
    }

    public Concentrador(Integer cantLineas, List<Linea> lineas) {
        //TODO se podria validar que la entrada en cant coincida con el tamanio de lineas
        this.cantLineas = cantLineas;
        this.lineas = lineas;
    }
    public List<Concentrador> tryFlip(){
        // esto es, si tengo un nodo en comun en las lineas del concentrador,
        // retorno otra combinacion, puede mejorar los costos de usuario
//        lineas.get(0).get
                return null;
    }

    private Integer[] getDelays() {
        Integer[] delays = new Integer[lineas.size()];
        for (int i = 0; i < lineas.size(); i++) {
            delays[i] = lineas.get(i).getDelay();
        }
        return delays;
    }

    public boolean isValido() {
        return true;
    }

    public Integer getCantLineas() {
        return cantLineas;
    }

    public List<Linea> getLineas() {
        return lineas;
    }

    private void genId() {
        lineas.sort(Comparator.comparing(Linea::getId));
        id = lineas.stream().map(Linea::getId).collect(Collectors.joining(":"));
    }

    public String getId() {
        if (id == null) {
            genId();
        }
        return id;
    }

    public boolean igualA(Concentrador other) {
        boolean encontre;
        for (Linea linea1 : lineas) {
            encontre = false;
            for (Linea linea2 : other.getLineas()) {
                if (linea1.igualA(linea2)) {
                    encontre = true;
                    break;
                }
            }
            if (!encontre) {
                return false;
            }
        }
        return true;
    }
}
