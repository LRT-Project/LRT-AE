
/**
 * Source https://gist.github.com/hallazzang/4e6abbb05ff2d3e168a87cf10691c4fb
 */
package uy.edu.fing.lrt.util.matrix;


import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.concurrent.Callable;

public class Matrix<T> {

    private T[][] data = null;
    private int rows;
    private int cols;

    private T defaultValue = null;

    public Matrix(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
    }

    public Matrix(Matrix<T> copy) {
        this.data = copy.data.clone();

        this.defaultValue = copy.defaultValue;
        this.rows = copy.rows;
        this.cols = copy.cols;
    }

    public void setDefaultValue(T defaultValue) {
        this.defaultValue = defaultValue;
    }

    public T getValueAt(int x, int y) {
        return safeGet(() -> data[x][y]);
    }

    public Matrix<T> setValueAt(int x, int y, T value) {
        if (data == null) {
            T[] arr = (T[]) Array.newInstance(value.getClass(), cols);
            data = (T[][]) Array.newInstance(arr.getClass(), rows);
            for (int i = 0; i < rows; i++) {
                data[i] = arr.clone();
            }
        }

        data[x][y] = value;
        return this;
    }

    public Matrix<T> transpose() {
        Matrix result = new Matrix<T>(cols, rows);

        for (int row = 0; row < rows; ++row) {
            for (int col = 0; col < cols; ++col) {
                result.setValueAt(col, row, data[row][col]);
            }
        }

        return result;
    }

    // Note: exclude_row and exclude_col starts from 1
    public Matrix<T> subMatrix(int exclude_row, int exclude_col) {
        Matrix result = new Matrix<T>(this.rows - 1, this.cols - 1);

        for (int row = 0, p = 0; row < this.rows; ++row) {
            if (row != exclude_row - 1) {
                for (int col = 0, q = 0; col < this.cols; ++col) {
                    if (col != exclude_col - 1) {
                        result.setValueAt(p, q, this.data[row][col]);
                        ++q;
                    }
                }
                ++p;
            }
        }

        return result;
    }

    public String stringify() {
        StringBuilder resultBuilder = new StringBuilder("[");

        for (T[] row : data) {
            resultBuilder.append("[");
            for (T value : row) {
                resultBuilder.append(value).append(" ");
            }
            resultBuilder.append("]\n");
        }
        resultBuilder.append("]");

        return resultBuilder.toString();
    }

    private T safeGet(Callable<T> call) {
        try {
            T value = call.call();
            return value == null ? defaultValue : value;
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public void show() {
        System.out.println("[");

        Arrays.stream(data).forEachOrdered(row -> {
            System.out.print("[");
            Arrays.stream(row).map(value -> safeGet(() -> value) + " ").forEachOrdered(System.out::print);
            System.out.println("]");
        });
        System.out.println("]");
    }
}