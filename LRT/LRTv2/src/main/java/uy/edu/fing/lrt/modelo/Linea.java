package uy.edu.fing.lrt.modelo;

import uy.edu.fing.lrt.controlador.PropiedadesControlador;
import uy.edu.fing.lrt.util.Logger;
import uy.edu.fing.lrt.util.MathUtil;
import uy.edu.fing.lrt.util.PropiedadesEnum;

import java.util.*;
import java.util.stream.Collectors;

public final class Linea {

    private List<Arista> tramos;
    private List<Nodo> nodos;
    private String id = null;
    private Integer delay = null;
    private Integer delayDirecto = null;
    private Integer frecuencia = null;

    public Linea(Linea copy) {
        this.id = copy.id;
        this.delay = copy.delay;
        this.delayDirecto = copy.delayDirecto;
        this.setFrecuencia(copy.frecuencia);
        this.tramos = new ArrayList<>(copy.tramos);
        this.nodos = new ArrayList<>(copy.nodos);
    }

    public Linea(List<Arista> tramos) {
        this.tramos = tramos;
        ordenarAristas();
    }

    public List<Arista> getTramos() {
        return tramos;
    }

    private void ordenarAristas() {
        try {
            if (tramos.size() == 1) {
                Nodo[] nodos = {tramos.get(0).getNodoA(), tramos.get(0).getNodoB()};
                this.nodos = Arrays.asList(nodos);
                return;
            }
            final Map<Integer, Nodo> cache = new HashMap<>();
            final List<Arista> nuevoTramo = new ArrayList<>();
            final List<Nodo> nodosOrdenados = new ArrayList<>();

            for (Arista arista : tramos) {
                cache.put(arista.getIdNodoA(), arista.getNodoA());
                cache.put(arista.getIdNodoB(), arista.getNodoB());
            }

            Nodo pivotN = null;
            Arista pivotA = null;
            List<Nodo> nodosUsados = new ArrayList<>(cache.values());
            nodosUsados.sort(Comparator.comparing(Nodo::getNombre));
            this.nodos = nodosUsados;
            for (Nodo value : nodosUsados) {
                List<Arista> collect = tramos.stream()
                        .filter(a -> a.getIdNodoA() == value.getNombre() || a.getIdNodoB() == value.getNombre())
                        .collect(Collectors.toList());
                if (collect.size() == 1) {
                    pivotN = value;
                    pivotA = collect.get(0);
                    break;
                }
            }

            for (int i = 0; i < cache.size(); i++) {
                nodosOrdenados.add(pivotN);
                if (pivotA.getIdNodoA() == pivotN.getNombre()) {
                    pivotN = pivotA.getNodoB();
                } else {
                    pivotN = pivotA.getNodoA();
                }

                for (Arista a : tramos) {
                    if (a.getId().equals(pivotA.getId())) {
                        continue;
                    }
                    if (a.getIdNodoA() == pivotN.getNombre() || a.getIdNodoB() == pivotN.getNombre()) {
                        nuevoTramo.add(pivotA);
                        pivotA = a;
                        break;
                    }
                }
            }
            nodos = nodosOrdenados;
            tramos = nuevoTramo;
        } catch (Exception e) {
            Logger.error("No se pudo ORDENAR", e);
        }
    }

    public List<Integer> getNodos() {
        return nodos.stream().map(Nodo::getNombre).collect(Collectors.toList());
    }

    public String getId() {
        if (id == null) {
            genId();
        }
        return id;
    }

    public Integer getFrecuencia() {
        return frecuencia;
    }

    public void setFrecuencia(Integer frecuencia) {
        this.frecuencia = frecuencia;
    }

    public void addFrecuenciaToNodos() {
        nodos.forEach(n -> n.setFrecuencia(this.frecuencia));
    }

    public Integer getLargo() {
        return tramos.stream().mapToInt(Arista::getLargo).sum();
    }

    public Integer getDelay() {
        if (delay == null) {
            calcularDelay();
        }
        return delay;
    }

    public Integer getDelayDirecto() {
        if (delayDirecto == null) {
            calcularDelay();
        }
        return delayDirecto;
    }

    private void genId() {
        tramos.sort(Comparator.comparing(Arista::getId));
        id = tramos.stream().map(Arista::getId).map(Object::toString).collect(Collectors.joining("."));
    }

    private void calcularDelay() {
        final Integer tiempo = PropiedadesControlador.getIntProperty(PropiedadesEnum.ESTACION_TIEMPO_INTERCAMBIO);
        final Integer velocidad = PropiedadesControlador.getIntProperty(PropiedadesEnum.BUS_VELOCIDAD);
        final Double aceleracion = PropiedadesControlador.getDoubleProperty(PropiedadesEnum.BUS_ACELERACION);

        this.delay = tramos.stream().mapToInt(Arista::getDelay).sum();
        int largoTotal = tramos.stream().mapToInt(Arista::getLargo).sum();

        this.delayDirecto = tiempo + MathUtil.calcularTiempoPorDistancia(velocidad, aceleracion, largoTotal);
    }

    public boolean igualA(Linea other) {
        return this.getId().equals(other.getId());
    }

}
