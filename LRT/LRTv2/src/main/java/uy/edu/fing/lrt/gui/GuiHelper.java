package uy.edu.fing.lrt.gui;

import uy.edu.fing.lrt.controlador.AEControlador;
import uy.edu.fing.lrt.controlador.ProblemaControlador;
import uy.edu.fing.lrt.controlador.PropiedadesControlador;
import uy.edu.fing.lrt.modelo.*;
import uy.edu.fing.lrt.test.Parada;
import uy.edu.fing.lrt.util.PropiedadesEnum;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class GuiHelper {

    private static javax.swing.JLabel lblEstado;
    private static javax.swing.JPanel mapa;
    private static List<Arista> individuo = null;

    private GuiHelper() {
    }

    public static void setLblEstado(JLabel lblEstado) {
        GuiHelper.lblEstado = lblEstado;
    }

    public static void setMapa(JPanel mapa) {
        GuiHelper.mapa = mapa;
    }

    public static void setIndividuo(List<Arista> individuo) {
        GuiHelper.individuo = individuo;
    }

    public static List<Arista> getIndividuo() {
        return individuo;
    }

    public static void dibujarNodos() {
        Graphics g = mapa.getGraphics();
        resetStroke(g);
        mapa.paintComponents(dibujarParadasyNodos(g));
    }

    public static void dibujarAristas(List<Arista> aristas, Color color, int px) {
        GuiHelper.setIndividuo(aristas);
        Graphics g = mapa.getGraphics();
        clearAristas(g);
        dibujarAristas(g);
        dibujarAristas(g, aristas, color, px);
        dibujarParadasyNodos(g);
        mapa.paintComponents(g);
    }

    public static void dibujarLinas(java.util.List<Linea> lineas) {
        Graphics g = mapa.getGraphics();
        //Graphics g = new DebugGraphics();
        clearAristas(g);
        dibujarAristas(g);
        Color[] colors = PropiedadesControlador.getColorsProperties(PropiedadesEnum.COLORES);
        for (int i = 0; i < lineas.size(); i++) {
            dibujarAristas(g, lineas.get(i).getTramos(), colors[i], 2);
        }
        dibujarParadasyNodos(g);
        mapa.paintComponents(g);
    }

    public static void dibujarLinas(Individuo i, Boolean[] consentradoresActivos) {
        Concentrador[] concentradores = i.getConcentradores();
        java.util.List<Linea> lineas = new ArrayList<>();
        for (int j = 0; j < concentradores.length; j++) {
            if (consentradoresActivos[j]) {
                lineas.addAll(concentradores[j].getLineas());
            }
        }
        dibujarLinas(lineas);
    }

    public static void clearAristas() {
        Graphics g = mapa.getGraphics();
        clearAristas(g);
        mapa.paintComponents(g);
    }

    public static void dibujarAristas() {
        Graphics g = mapa.getGraphics();
        clearAristas(g);
        dibujarAristas(g);
        dibujarParadasyNodos(g);
        mapa.paintComponents(g);
    }

    public static void dibujarBest(Boolean[] consentradoresActivos) {
        lblEstado.setText("");
        Individuo best = AEControlador.getInstance().getBest();
        if (best != null) {
            individuo = best.getAristas();
            String tiempo = AEControlador.getInstance().getTime();
            int gen = AEControlador.getInstance().getGeneracion();
            String frecs = best.getLineas().stream().map(l -> l.getFrecuencia() + "").collect(Collectors.joining("-"));

            lblEstado.setText("Generacion: " + gen + " | Frecuencias: " + frecs + " | Costo Total: " + best.getCosto() + " | Costo USD: " + best.getCostoDinero() + " | Tiempo: " + tiempo);
            dibujarLinas(best, consentradoresActivos);
        } else {
            if (individuo != null) {
                GuiHelper.dibujarAristas(individuo, Color.MAGENTA, 2);
            }
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////

    private static Graphics resetStroke(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(1));
        return g;
    }

    private static Graphics dibujarNodos(Graphics g) {
        resetStroke(g);
        Boolean debug = PropiedadesControlador.getBoolProperty(PropiedadesEnum.DEBUG);

        ProblemaControlador controlador = ProblemaControlador.getInstance();

        double propY = ((double) mapa.getSize().getHeight() - 20) / controlador.getHeight();
        double propX = ((double) mapa.getSize().getWidth() - 20) / controlador.getWidth();

        final Integer cantFuente = PropiedadesControlador.getIntProperty(PropiedadesEnum.CANT_FUENTES);
        final Integer cantSum = PropiedadesControlador.getIntProperty(PropiedadesEnum.CANT_SUMIDEROS);

        java.util.List<Nodo> nodos = ProblemaControlador.getInstance().getNodos();
        nodos.forEach((nodo) -> {
            if (nodo.getNombre() <= cantSum) {
                g.setColor(Color.BLUE);
            } else {
                if (nodo.getNombre() <= (cantSum + cantFuente)) {
                    g.setColor(Color.RED);
                } else {
                    g.setColor(Color.YELLOW);
                }
            }
            final int x = ((int) (nodo.getCoordX() * propX)) + 10;
            final int y = (int) mapa.getSize().getHeight() - (((int) (nodo.getCoordY() * propY))) - 10;

            g.drawOval(x - 3, y - 3, 7, 7);
            g.fillOval(x - 3, y - 3, 7, 7);

            if (debug) {
                g.drawString(nodo.getNombre() + "", x, y);
            }
        });

        return g;
    }

    private static Graphics dibujarParadasyNodos(Graphics g) {
        resetStroke(g);
        Boolean debug = PropiedadesControlador.getBoolProperty(PropiedadesEnum.DEBUG);

        ProblemaControlador controlador = ProblemaControlador.getInstance();

        double propY = ((double) mapa.getSize().getHeight() - 20) / controlador.getHeight();
        double propX = ((double) mapa.getSize().getWidth() - 20) / controlador.getWidth();

        final Integer cantFuente = PropiedadesControlador.getIntProperty(PropiedadesEnum.CANT_FUENTES);
        final Integer cantSum = PropiedadesControlador.getIntProperty(PropiedadesEnum.CANT_SUMIDEROS);

        java.util.List<Nodo> nodos = ProblemaControlador.getInstance().getNodos();
        java.util.List<Parada> paradas = ProblemaControlador.getInstance().getParadas();

        nodos.forEach((nodo) -> {
            if (nodo.getNombre() <= cantSum) {
                g.setColor(Color.BLUE);
            } else {
                if (nodo.getNombre() <= (cantSum + cantFuente)) {
                    g.setColor(Color.RED);
                } else {
                    g.setColor(Color.YELLOW);
                }
            }
            final int x = ((int) (nodo.getCoordX() * propX)) + 10;
            final int y = (int) mapa.getSize().getHeight() - (((int) (nodo.getCoordY() * propY))) - 10;

            g.drawOval(x - 3, y - 3, 7, 7);
            g.fillOval(x - 3, y - 3, 7, 7);

            if (debug) {
                g.drawString(nodo.getNombre() + "", x, y);
            }
        });

        g.setColor(Color.WHITE);
        paradas.forEach((parada) -> {

            final int x = ((int) (parada.getCoordX().intValue() * propX)) + 10;
            final int y = (int) mapa.getSize().getHeight() - (((int) (parada.getCoordY().intValue() * propY))) - 10;

            g.drawOval(x - 1, y - 1, 1, 1);
            g.fillOval(x - 1, y - 1, 1, 1);

        });

//        java.util.List<Parada> coordEspeciales = ProblemaControlador.getInstance().getCoordEspeciales();
//        g.setColor(Color.green);
//        coordEspeciales.forEach((parada) -> {
//
//            final int x = ((int) (parada.getCoordX().intValue() * propX)) + 10;
//            final int y = (int) mapa.getSize().getHeight() - (((int) (parada.getCoordY().intValue() * propY))) - 10;
//
//            g.drawOval(x - 3, y - 3, 7, 7);
//            g.fillOval(x - 3, y - 3, 7, 7);
//
//        });
        return g;
    }

    private static Graphics dibujarAristas(Graphics g, List<Arista> aristas, Color color, int px) {
        Boolean debug = PropiedadesControlador.getBoolProperty(PropiedadesEnum.DEBUG);

        ProblemaControlador controlador = ProblemaControlador.getInstance();

        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(px));

        double propY = ((double) mapa.getSize().getHeight() - 20) / controlador.getHeight();
        double propX = ((double) mapa.getSize().getWidth() - 20) / controlador.getWidth();

        g.setColor(color);
        aristas.forEach((a) -> {
            final int x1 = ((int) (a.getNodoA().getCoordX() * propX)) + 10;
            final int y1 = (int) mapa.getSize().getHeight() - (((int) (a.getNodoA().getCoordY() * propY))) - 10;

            final int x2 = ((int) (a.getNodoB().getCoordX() * propX)) + 10;
            final int y2 = (int) mapa.getSize().getHeight() - (((int) (a.getNodoB().getCoordY() * propY))) - 10;

            g2.draw(new Line2D.Float(x1, y1, x2, y2));
            if (debug) {
                g.drawString(a.getId() + "", (x1 + x2) / 2, (y1 + y2) / 2);
            }
        });
        return g;
    }

    private static Graphics dibujarAristas(Graphics g) {
        java.util.List<Arista> aristas = ProblemaControlador.getInstance().getAristas();
        dibujarAristas(g, aristas, Color.DARK_GRAY, 1);
        return g;
    }

    private static Graphics clearAristas(Graphics g) {
        java.util.List<Arista> aristas = ProblemaControlador.getInstance().getAristas();
        dibujarAristas(g, aristas, mapa.getBackground(), 10);
        return g;
    }
}
