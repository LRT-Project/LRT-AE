package uy.edu.fing.lrt.modelo;

import uy.edu.fing.lrt.controlador.ProblemaControlador;
import uy.edu.fing.lrt.util.GlpkUtil;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public final class Generador {

    //Metodos estaticos
    private Generador() {
    }

    private void metodo() {
        Individuo individuo = Generador.sol1();
        Integer delay1 = (new Linea(GlpkUtil.SPP(individuo.getAristas(), 5, 4, Arista::getDelay))).getDelay() / 60;
        Integer delay2 = (new Linea(GlpkUtil.SPP(individuo.getAristas(), 5, 3, Arista::getDelay))).getDelay() / 60;
        Integer delay3 = (new Linea(GlpkUtil.SPP(individuo.getAristas(), 7, 3, Arista::getDelay))).getDelay() / 60;
        Integer delay4 = (new Linea(GlpkUtil.SPP(individuo.getAristas(), 7, 6, Arista::getDelay))).getDelay() / 60;
        Integer delay5 = (new Linea(GlpkUtil.SPP(individuo.getAristas(), 6, 2, Arista::getDelay))).getDelay() / 60;
        System.out.println();
    }

    private static Arista find(int a, int b) {
        return ProblemaControlador.getInstance().getArista(a, b);
    }

    private static Arista find(int a) {
        return ProblemaControlador.getInstance().getArista(a);
    }

    private static Linea build(int[] idAristas) {
        List<Arista> aristas = Arrays.stream(idAristas).mapToObj(Generador::find).collect(Collectors.toList());
        return new Linea(aristas);
    }

    private static Concentrador build(int[]... idAristas) {
        int size = idAristas.length;
        List<Linea> lineas = Arrays.stream(idAristas).map(Generador::build).collect(Collectors.toList());
        return new Concentrador(size, lineas);
    }

    private static Individuo buildI(int[] sizes, int[]... idAristas) {
        int start = 0;
        int end;

        Concentrador[] c = new Concentrador[sizes.length];
        for (int i = 0, sizesLength = sizes.length; i < sizesLength; i++) {
            int size = sizes[i];
            end = start + size;
            c[i] = build(Arrays.copyOfRange(idAristas, start, end));
            start = end;
        }

        return new Individuo(c);
    }

    public static Individuo sol1() {
        return buildI(new int[]{2, 2, 3, 2},
                new int[]{18, 32, 64, 6},
                new int[]{16, 29, 65, 21, 22, 68, 70, 72, 83, 10},//ojo aca, no es 10, 12

                new int[]{20, 46, 48, 50, 52, 79, 11},
                new int[]{19, 45, 49, 51, 7},

                new int[]{21, 65, 62, 64, 6},
                new int[]{22, 68, 70, 72, 83, 10},
                new int[]{23, 89, 86, 87, 13},

                new int[]{27, 44, 45, 49, 52, 79, 11},
                new int[]{26, 42, 50, 51, 7}

        );
    }

    public static Individuo sol2() {
        return buildI(new int[]{2, 2, 3, 2},
                new int[]{18, 32, 64, 6},
                new int[]{16, 29, 65, 21, 22, 68, 70, 72, 83, 12},//ojo aca, no es 10, 12

                new int[]{20, 46, 48, 50, 52, 79, 11},
                new int[]{19, 45, 49, 51, 7},

                new int[]{21, 65, 62, 64, 6},
                new int[]{22, 68, 70, 72, 83, 12},
                new int[]{23, 89, 86, 87, 13},

                new int[]{27, 44, 45, 49, 52, 79, 11},
                new int[]{26, 42, 50, 51, 7}

        );
    }
}
