package uy.edu.fing.lrt.util;

import uy.edu.fing.lrt.controlador.PropiedadesControlador;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public final class Logger {

    private static final String hostName = getHostName();
    private static final String homeDir = System.getProperty("user.home");
    private static final String fileSeparator = System.getProperty("file.separator");
    public static String NAME_LOGGER = "LOGGER.log";

    private static long initTime = System.currentTimeMillis();

    public static String getHostName() {
        String tmp;
        try {
            tmp = InetAddress.getLocalHost().getHostName();
            if (tmp != null) {
                return tmp;
            }
        } catch (Exception ignore) {
        }
        return "";
    }

    public static void initLogger() {

        final String filePath = homeDir + fileSeparator + NAME_LOGGER;
        try {
            initTime = System.currentTimeMillis();

            File file = new File(filePath);
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static synchronized void write(String text) {
        final String filePath = homeDir + fileSeparator + NAME_LOGGER;
        try {
            final String now = (System.currentTimeMillis() - initTime) + "\t";
            final String msg = now + text + "\n";
            File file = new File(filePath);
            if (file.exists()) {
                Files.write(Paths.get(filePath), msg.getBytes(), StandardOpenOption.APPEND);
            } else {
                Files.write(Paths.get(filePath), msg.getBytes(), StandardOpenOption.CREATE_NEW);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void info(String msg) {
        System.out.println("INFO : " + msg);
        write("INFO\t" + msg);
    }

    public static void debug(String msg) {
        Boolean debugEneabled = PropiedadesControlador.getBoolProperty(PropiedadesEnum.DEBUG);
        if (debugEneabled) {
            System.out.println("DEBUG: " + msg);
            write("DEBUG\t" + msg);
        }
    }

    public static void error(String msg) {
        System.err.println("ERROR: " + msg);
        write("ERROR\t" + msg);
    }

    public static void error(String msg, Throwable t) {
        System.err.println("ERROR: " + msg + " : " + t.getMessage());

        write("ERROR\t" + msg + "\t" + t.getMessage());
        t.printStackTrace();
    }

}
