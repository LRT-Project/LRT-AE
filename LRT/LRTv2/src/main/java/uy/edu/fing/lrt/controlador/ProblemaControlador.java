package uy.edu.fing.lrt.controlador;

import uy.edu.fing.lrt.modelo.*;
import uy.edu.fing.lrt.test.Parada;
import uy.edu.fing.lrt.util.Random;
import uy.edu.fing.lrt.util.*;
import uy.edu.fing.lrt.util.matrix.Matrix;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class ProblemaControlador {

    private static ProblemaControlador INSTANCIA = null;
    private int height = 0;
    private int width = 0;

    private HashMap<Integer, Nodo> nodos;
    private HashMap<Integer, Arista> aristasId;
    private HashMap<Integer, Arista> aristasPair;

    private HashMap<Integer, Parada> paradasOriginales;
    private List<Parada> coordEspeciales;

    private Matrix<Double> matrizOD;
    private Matrix<Integer> matrizDelaysPuntoAPunto;

    private ProblemaControlador() {
        nodos = new HashMap<>();
        aristasPair = new HashMap<>();
        aristasId = new HashMap<>();
        paradasOriginales = new HashMap<>();

        matrizOD = new Matrix<>(6150, 6150);
        matrizOD.setDefaultValue(0d);

        System.out.println("OK");
    }

    public List<Parada> getCoordEspeciales() {
        if (coordEspeciales == null) {
            coordEspeciales = new ArrayList<>();
            coordEspeciales.add(new Parada("10000\t3502646\t5611050"));
            coordEspeciales.add(new Parada("10003\t3453314\t5616550"));
            coordEspeciales.add(new Parada("10001\t3445756\t5613413"));
            coordEspeciales.add(new Parada("10002\t3451896\t5601352"));
        }
        return coordEspeciales;
    }

    public static ProblemaControlador getInstance() {
        if (INSTANCIA == null) {
            INSTANCIA = new ProblemaControlador();
        }
        return INSTANCIA;
    }

    public void addNodo(Nodo n) {
        Integer nombre = n.getNombre();
        if (nodos.containsKey(nombre)) {
            throw new RuntimeException("Nodo ya en la lista: " + nombre);
        }
        if (n.getCoordX() > width) {
            width = n.getCoordX();
        }
        if (n.getCoordY() > height) {
            height = n.getCoordY();
        }
        nodos.put(nombre, n);
    }

    public void addArista(Arista a) {
        Integer id = aristasId.size() + 1;
        a.setId(id);
        Nodo nodoA = nodos.get(a.getIdNodoA());
        Nodo nodoB = nodos.get(a.getIdNodoB());
        nodoA.addAdyacente(nodoB);
        nodoB.addAdyacente(nodoA);
        a.setNodes(nodoA, nodoB);
        aristasId.put(id, a);
        aristasPair.put(pairId(a), a);
    }

    public void addParada(Parada p) {
        paradasOriginales.put(p.getNombre(), p);
    }

    public Matrix<Integer> obtenerMatrizDemanda(List<Nodo> nodos) {

        int cantNodos = cantNodos();

        Matrix<Integer> matriz = new Matrix<>(cantNodos + 1, cantNodos + 1);
        matriz.setDefaultValue(0);

        Map<Integer, Integer> paradas = asignarParadasAEstaciones(nodos);

        paradas.forEach((k_i, v_i) ->
                paradas.forEach((k_j, v_j) ->
                        matriz.setValueAt(v_i, v_j,
                                matriz.getValueAt(v_i, v_j) + matrizOD.getValueAt(k_i - 1, k_j - 1).intValue())));

        IntStream.rangeClosed(0, cantNodos).forEachOrdered(i -> matriz.setValueAt(i, i, null));

        //Borro la demanda en el centro
        for (int i = 1; i <= 3; i++) {
            for (int j = i; j <= 3; j++) {
                matriz.setValueAt(i, j, null);
                matriz.setValueAt(j, i, null);
            }
        }
        return matriz;
    }


    private Map<Integer, Integer> asignarParadasAEstaciones(List<Nodo> nodos) {
        Map<Integer, Integer> result = new HashMap<>();
        paradasOriginales.values().stream()
                .filter(s -> s.getNombre() < 6150).forEach(e -> {

            Nodo nodo = nodos.stream().min(Comparator.comparing(o -> MathUtil.dist(o, e))).get();
            result.put(e.getNombre(), nodo.getNombre());
        });
        return result;
    }

    public void addODvalue(String[] splited) {
        matrizOD.setValueAt(Integer.parseInt(splited[0]), Integer.parseInt(splited[1]), Double.parseDouble(splited[2]));
    }

    private void preCalcularMatrizDelay() {
        int cantNodos = cantNodos();
        matrizDelaysPuntoAPunto = new Matrix<>(cantNodos + 1, cantNodos + 1);
        matrizDelaysPuntoAPunto.setDefaultValue(0);

        for (int i = 1; i <= cantNodos; i++) {
            for (int j = i + 1; j <= cantNodos; j++) {

                List<Arista> spp = GlpkUtil.SPP(getAristas(), i, j, Arista::getLargo);
                Linea linea = new Linea(spp);
                matrizDelaysPuntoAPunto.setValueAt(i, j, linea.getDelayDirecto());
                matrizDelaysPuntoAPunto.setValueAt(j, i, linea.getDelayDirecto());
            }
        }
    }

    private static Map<String, Integer[]> chacheFrec = new HashMap<>();

    private int cacheHit = 0;

    public void calculoDeFrecuencias(Individuo individuo) {
        String id = individuo.getId();
        Integer[] frec;
        if (chacheFrec.containsKey(id)) {
            cacheHit++;
            frec = chacheFrec.get(id);
        } else {
            frec = calculoDeFrecuencias_(individuo);
            if (!PropiedadesControlador.getBoolProperty(PropiedadesEnum.DEBUG)) {
                chacheFrec.put(id, frec);
            }
        }

        for (int i = 0, frecLength = frec.length; i < frecLength; i++) {
            individuo.getLinea(i).setFrecuencia(frec[i]);
        }
    }

    //TODO: evaluar modelo matematico para el calculo de frecuencias
    private Integer[] calculoDeFrecuencias_(Individuo individuo) {
        try {
            Logger.info("Calculo de frecuencia");

            if (PropiedadesControlador.getBoolProperty(PropiedadesEnum.DEBUG)) {
                return IntStream.range(0, 9).mapToObj(i -> Random.beetwen(1, 15)).toArray(Integer[]::new);
            }

            Integer[] frec = {1, 1, 1, 1, 1, 1, 1, 1, 1};
            Integer[] demandasMax = {0, 0, 0, 0, 0, 0, 0, 0, 0};
            Map<Integer, Integer> frecuenciaTramo = new HashMap<>();
            Map<Integer, Integer> demandaTramo = new HashMap<>();

            List<Arista> aristasIndividuo = individuo.getAristas();
            List<Nodo> nodosIndividuo = individuo.getNodos();

            //Calculo la matrix de demanda
            Matrix<Integer> matrixDemandaIndividuo = obtenerMatrizDemanda(nodosIndividuo);

            //Calculo la demanda por hora en cada tramo
            int lineaId = 0;
            Concentrador[] concentradores = individuo.getConcentradores();
            for (Concentrador concentrador : concentradores) {
                //Obtengo los primeros tramos de todas las lineas del concentrador
                Map<String, Arista> primerosTramos = new HashMap<>();
                concentrador.getLineas().forEach(l -> primerosTramos.put(l.getId(), l.getTramos().get(0)));

                for (Linea linea : concentrador.getLineas()) {
                    List<Integer> nodos = linea.getNodos();
                    Collections.reverse(nodos);
                    for (Integer nodoOrigen : nodos) {//nodos de la linea
                        for (Nodo nodoDestino : nodosIndividuo) {
                            //FIXME no chequea que no regrese, espero mas gente en los trenes
                            if (nodoDestino.getNombre() == nodoOrigen) {
                                continue;
                            }
                            Integer demanda = matrixDemandaIndividuo.getValueAt(nodoOrigen, nodoDestino.getNombre());
                            List<Arista> tramosProhibidos = primerosTramos.entrySet().stream().filter(e -> !e.getKey().equals(linea.getId())).map(Map.Entry::getValue).collect(Collectors.toList());
                            Linea lineaDemanda = new Linea(GlpkUtil.SPP(ListUtil.restaDeAristas(aristasIndividuo, tramosProhibidos), nodoOrigen, nodoDestino.getNombre(), Arista::getLargo));
                            for (Arista tramo : lineaDemanda.getTramos()) {
                                Integer demandaAcc = demandaTramo.getOrDefault(tramo.getId(), 0);
                                demandaTramo.put(tramo.getId(), demandaAcc + demanda);
                            }
                        }
                    }
                    lineaId++;
                }
            }

            //Busco la mejor combinacion de frecuencia para una solucion
            boolean ok = false;
            while (!ok) {
                //Calculo la Frecuenca disponible en cada tramo
                frecuenciaTramo.clear();
                lineaId = 0;
                for (Concentrador concentrador : concentradores) {
                    for (Linea linea : concentrador.getLineas()) {
                        for (Arista tramo : linea.getTramos()) {
                            //Acumulo frecuencias del tramo
                            Integer frecTramo = frecuenciaTramo.getOrDefault(tramo.getId(), 0);
                            frecuenciaTramo.put(tramo.getId(), frecTramo + frec[lineaId]);
                        }
                        lineaId++;
                    }
                }

                lineaId = 0;
                for (Concentrador concentrador : concentradores) {
                    for (Linea linea : concentrador.getLineas()) {
                        int demandaMaxLinea = 0;
                        for (Arista tramo : linea.getTramos()) {
                            //Acumulo frecuencias del tramo
                            Integer frecTramo = frecuenciaTramo.get(tramo.getId());
                            Integer demTramo = demandaTramo.getOrDefault(tramo.getId(), 0);
                            int demReal = demTramo / frecTramo;
                            if (demReal > demandaMaxLinea) {
                                demandaMaxLinea = demReal;
                            }
                        }
                        demandasMax[lineaId] = demandaMaxLinea;
                        lineaId++;
                    }
                }

                final Integer capacidad = PropiedadesControlador.getIntProperty(PropiedadesEnum.CAPACIDAD_TREN);
                Integer max = Arrays.stream(demandasMax).max(Comparator.naturalOrder()).get();
                if (max < capacidad) {
                    ok = true;
                } else {
                    Integer idMax = -1;
                    for (int i = 0, demandasMaxLength = demandasMax.length; i < demandasMaxLength; i++) {
                        if (max.equals(demandasMax[i])) {
                            idMax = i;
                            break;
                        }
                    }
                    frec[idMax]++;
                }

            }

            return frec;
        } catch (Exception e) {
            Logger.error("Error en frecuencia", e);
            return null;
        }
    }


    public Integer calculoDeCostoUsuario(Individuo individuo) {
        Double velocidad = PropiedadesControlador.getDoubleProperty(PropiedadesEnum.BUS_VELOCIDAD_PROMEDIO_ACTUAL);
        Double acumulado = 0D;

        individuo.getNodos().forEach(Nodo::clearFrecuencia);
        individuo.getLineas().forEach(Linea::addFrecuenciaToNodos);

        for (int i = 0; i < 6151; i++) {
            for (int j = i + 1; j < 6151; j++) {
                Double demanda = matrizOD.getValueAt(i, j);
                if (demanda == 0) {
                    continue;
                }

                Parada paradaO = paradasOriginales.get(i);
                Parada paradaD = paradasOriginales.get(j);
                if (paradaD == null || paradaO == null) {
                    continue;
                }
                //De todos los nodos
                Nodo estacionO = nodos.entrySet().stream().map(Map.Entry::getValue).min(Comparator.comparing(o -> MathUtil.dist(o, paradaO))).get();
                Nodo estacionD = nodos.entrySet().stream().map(Map.Entry::getValue).min(Comparator.comparing(o -> MathUtil.dist(o, paradaD))).get();
                double delayOptimo = velocidad * (MathUtil.dist(estacionO, paradaO) + MathUtil.dist(estacionD, paradaD)) / 3.6 + matrizDelaysPuntoAPunto.getValueAt(estacionO.getNombre(), estacionD.getNombre());
                //De los nodos de la solucion actual
                Nodo estacionIndO = individuo.getNodos().stream().min(Comparator.comparing(o -> MathUtil.dist(o, paradaO))).get();
                Nodo estacionIndD = individuo.getNodos().stream().min(Comparator.comparing(o -> MathUtil.dist(o, paradaD))).get();
                Linea lineaDemanda = new Linea(GlpkUtil.SPP(individuo.getAristas(), estacionIndO.getNombre(), estacionIndD.getNombre(), Arista::getLargo));
                double delayIndividuo = velocidad * (MathUtil.dist(estacionIndO, paradaO) + MathUtil.dist(estacionIndD, paradaD)) / 3.6 + lineaDemanda.getDelay();

                double frec = Math.max(estacionIndO.getFrecuencia(), estacionIndD.getFrecuencia());
                acumulado += (1800 / frec + delayIndividuo / delayOptimo) * demanda;
            }
        }

        return acumulado.intValue();
    }

    public int cantNodos() {
        return nodos.size();
    }

    public int cantAristas() {
        return aristasId.size();
    }

    public void datosCargados() {
        preCalcularMatrizDelay();
    }

    public void check() {
        System.out.println("cheking");
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public List<Nodo> getNodos() {
        return (new ArrayList<>(nodos.values()));
    }

    public List<Parada> getParadas() {
        return (new ArrayList<>(paradasOriginales.values()));
    }

    public List<Arista> getAristas() {
        return (new ArrayList<>(aristasId.values()));
    }

    private Integer pairId(Arista a) {
        return pairId(a.getIdNodoA(), a.getIdNodoB());
    }

    private Integer pairId(int idNodoA, int idNodoB) {
        if (idNodoA > idNodoB) {
            return idNodoA * cantNodos() + idNodoB;
        }
        return idNodoB * cantNodos() + idNodoA;
    }

    public Arista getArista(int nodoA, int nodoB) {
        return aristasPair.get(pairId(nodoA, nodoB));
    }

    public Arista getArista(int idArista) {
        return aristasId.get(idArista);
    }

    public Nodo getNodo(int idNodo) {
        return nodos.get(idNodo);
    }

    public void limpiar() {
        nodos = new HashMap<>();
        limpiarAristas();
    }

    public void limpiarAristas() {
        aristasId = new HashMap<>();
        aristasPair = new HashMap<>();
    }
}
