package uy.edu.fing.lrt.test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Histograma {

    private final static String WORK_DIR = "C:\\Tesina\\";
    private final static String MATRIZ_DIR = "nebula\\";
    private final static String FILE1 = "ventas2015_10_sort.csv";
    private final static String FILE2 = "ventas_sin_2015_10_sort.csv";

    public static void main(String[] args) throws Exception {
        leerMatrizCon();
        //leerMatrizSin();
    }

    private static void leerMatrizCon() throws Exception {
        leerMatriz(FILE1);
    }

    private static void leerMatrizSin() throws Exception {
        leerMatriz(FILE2);
    }

    private static void leerMatriz(String file) throws Exception {
        int[] horas = new int[24];
        Map<String, LineaBase> recorridosTemprano = new HashMap<>();
        Map<String, LineaBase> recorridosTarde = new HashMap<>();

        String lineTmp = "";
        String hora = "";

        try (BufferedReader br = new BufferedReader(new FileReader(WORK_DIR + MATRIZ_DIR + file))) {
            for (String line; (line = br.readLine()) != null; ) {
                if ("thash,secuencial_operacion_tarjeta,secuencial_multitramo,tramo,pasajeros,parada_origen,parada_destino,recorrido,fecha_evento,latitud,longitud,empresa,anulado"
                        .equals(line)) {
                    continue;
                }
                if ("tramo,pasajeros,parada_origen,parada_destino,recorrido,fecha_evento,latitud,longitud,empresa,anulado"
                        .equals(line)) {
                    continue;
                }
                lineTmp = line;

                LineaBase base = new LineaBase(line);
                if (!base.getAnulado().equals("N")) {
                    continue;
                }

                procesarRecorrido(recorridosTemprano, recorridosTarde, base);

                hora = base.fecha_evento.substring(12, 14);
                Integer intHora = Integer.valueOf(hora);
                horas[intHora]++;

            }
        } catch (Exception e) {
            System.out.println(lineTmp);
            System.err.println(e.getMessage());
        }
        //recorridosTarde.entrySet().stream().filter(e->!recorridosTemprano.get(e.getKey()).equals(e.getValue())).collect(Collectors.toList())
        for (int i = 0; i < horas.length; i++) {
            System.out.println(i + ": " + horas[i]);
        }


        try (PrintWriter writer = new PrintWriter(WORK_DIR + "HistogramaVelocidad.csv", "UTF-8")) {
            for (Map.Entry<String, LineaBase> entry : recorridosTarde.entrySet()) {
                LineaBase tarde = entry.getValue();
                LineaBase temprano = recorridosTemprano.get(entry.getKey());
                String newLine = entry.getKey() + "\t " +
                        tarde.fecha_evento + "\t " +
                        tarde.getLatitud() + "\t " +
                        tarde.getLongitud() + "\t " +
                        temprano.fecha_evento + "\t " +
                        temprano.getLatitud() + "\t " +
                        temprano.getLongitud();

                writer.println(newLine);
            }
        }
    }

    private static void procesarRecorrido(Map<String, LineaBase> temprano, Map<String, LineaBase> tarde, LineaBase linea) throws Exception {
        if (!temprano.containsKey(linea.recorrido)) {
            temprano.put(linea.recorrido, linea);
        }
        if (!tarde.containsKey(linea.recorrido)) {
            tarde.put(linea.recorrido, linea);
        }

        LineaBase tardeL = tarde.get(linea.recorrido);
        LineaBase tempranoL = temprano.get(linea.recorrido);
        if (linea.getFecha().after(tardeL.getFecha())) {
            tarde.put(linea.recorrido, linea);
        }
        if (linea.getFecha().before(tempranoL.getFecha())) {
            temprano.put(linea.recorrido, linea);
        }

    }

    private static class LineaBase {
        private String thash;
        private String secuencial_operacion_tarjeta;
        private String secuencial_multitramo;
        private String tramo;
        private String pasajeros;
        private String parada_origen;
        private String parada_destino;
        private String recorrido;
        private String fecha_evento;
        private String latitud;
        private String longitud;
        private String empresa;
        private String anulado;

        public LineaBase(String line) {
            String[] split = line.split(",");
            boolean completo = false;
            int offset = 0;
            if (split.length == 13) {
                completo = true;
                offset = 3;
            }
            if (completo) {
                thash = split[0].trim();
                secuencial_operacion_tarjeta = split[1].trim();
                secuencial_multitramo = split[2].trim();
            }
            tramo = split[0 + offset].trim();
            pasajeros = split[1 + offset].trim();
            parada_origen = split[2 + offset].trim();
            parada_destino = split[3 + offset].trim();
            recorrido = split[4 + offset].trim();
            fecha_evento = split[5 + offset].trim();
            latitud = split[6 + offset].trim();
            longitud = split[7 + offset].trim();
            empresa = split[8 + offset].trim();
            anulado = split[9 + offset].trim();
        }

        public String getThash() {
            return thash;
        }

        public String getSecuencial_operacion_tarjeta() {
            return secuencial_operacion_tarjeta;
        }

        public String getSecuencial_multitramo() {
            return secuencial_multitramo;
        }

        public String getTramo() {
            return tramo;
        }

        public String getPasajeros() {
            return pasajeros;
        }

        public String getParada_origen() {
            return parada_origen;
        }

        public String getParada_destino() {
            return parada_destino;
        }

        public String getRecorrido() {
            return recorrido;
        }

        public String getFecha_evento() {
            return fecha_evento;
        }

        public Date getFecha() throws ParseException {
            return (new SimpleDateFormat("\"dd/MM/yyyy HH:mm:ss\"")).parse(fecha_evento.trim());
        }

        public String getLatitud() {
            return latitud;
        }

        public String getLongitud() {
            return longitud;
        }

        public String getEmpresa() {
            return empresa;
        }

        public String getAnulado() {
            return anulado;
        }
    }
}
