package uy.edu.fing.lrt.test;


import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import java.util.List;

public class ProcesarMatriz {

    private final static int SIZE_X = 1074;
    private final static int SIZE_Y = 744;

    private final static String WORK_DIR = "C:\\Tesina\\";
    private final static String MATRIZ_DIR = "Matriz_OD\\";

    //private static int[][] matriz = new int[5604][5604];
    private static int[][] matriz = new int[6150][6150];

    public static void main(String[] args) throws Exception {
        int size = LectorNodos.getNodos().size();

        final List<Parada> nodos = LectorNodos.filtrarNodos();
        List<Parada> sig = LectorSIG.getNodos();

        Map<Integer, Integer> paradas = asignarParadasAEstaciones(nodos, sig);
        Object[] matriz10 = leerMatriz("10-auto.mat");

        int[][] matrizReducida = new int[size + 1][size + 1];
        paradas.forEach((k_i, v_i) ->
                paradas.forEach((k_j, v_j) ->
                        matrizReducida[v_i][v_j] += ((Integer[]) matriz10[k_i - 1])[k_j - 1]));


        toMAT(matrizReducida, "10-auto-agupada.mat");
        System.out.println("Finish");
    }

    public static Object[] leerMatriz(String name) throws Exception {
        //Integer[][] matriz = new Integer[6150][6150];
        List<Integer[]> matriz = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(WORK_DIR + MATRIZ_DIR + name))) {
            for (String line; (line = br.readLine()) != null; ) {
                final Integer[] row =
                        Arrays.stream(line.trim().split(" "))
                                .map(String::trim)
                                .map(Integer::valueOf)
                                .toArray(Integer[]::new);
                matriz.add(row);
            }
        } catch (FileNotFoundException fnfe) {
            System.err.println(fnfe.getMessage());
        }
        return matriz.toArray();
    }

    public static List<String> leerUbicacionesProcesadas() throws Exception {
        List<String> elementos = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(WORK_DIR + "datos\\ubicacionProcesada.txt"))) {
            for (String line; (line = br.readLine()) != null; ) {
                elementos.add(line);
            }
        } catch (FileNotFoundException fnfe) {
            System.err.println(fnfe.getMessage());
        }
        return elementos;
    }


    public static boolean[][] paradasToMatriz(List<String> paradas) throws Exception {

        boolean[][] matriz = new boolean[SIZE_X][SIZE_Y];

        final Double xProp = 30.8175195144;
        final Double yProp = 25.2991694915;

        final Double xShift = 554395.5453556540;
        final Double yShift = 6134610.9221502500;

        //final BufferedImage res = new BufferedImage(1210, 927, BufferedImage.TYPE_INT_RGB);
        for (String elemento : paradas) {
            final String[] split = elemento.split("\t");
            final Double xImg = (Double.valueOf(split[1]) - xShift) / xProp;
            final Double yImg = (Double.valueOf(split[2]) - yShift) / yProp;
            try {
                matriz[xImg.intValue()][926 - yImg.intValue()] = true;
            } catch (ArrayIndexOutOfBoundsException aiobe) {
                System.err.println("Valores: (" + xImg.intValue() + " - " + yImg.intValue() + ")");
            }
        }
        return matriz;
    }

    public static void drawDot(Graphics g, Parada n) {
        int x = n.getCoordX().intValue();
        int y = 2342 - n.getCoordY().intValue();
        g.drawOval(x - 3, y - 3, 7, 7);
        g.fillOval(x - 3, y - 3, 7, 7);
    }

    public static Double dist(Parada n1, Parada n2) {
        final double distX = n1.getCoordX() - n2.getCoordX();
        final double distY = n1.getCoordY() - n2.getCoordY();

        return Math.sqrt(distX * distX + distY * distY);
    }

    public static Map<Integer, Integer> asignarParadasAEstaciones(List<Parada> nodos, List<Parada> sig) throws Exception {
        Map<Integer, Integer> result = new HashMap<>();
        sig.stream().filter(s -> s.getNombre() < 6150).forEach(e -> {
            Parada nodo = nodos.stream().min(Comparator.comparing(o -> dist(o, e))).get();
            result.put(e.getNombre(), nodo.getNombre());
        });
        return result;
    }

    public static void mergeToImg(List<Parada> nodos, List<Parada> sig) throws Exception {

        int SIZE_X = 3726;
        int SIZE_Y = 2343;

        final BufferedImage res = new BufferedImage(SIZE_X, SIZE_Y, BufferedImage.TYPE_INT_RGB);
        Graphics graphics = res.getGraphics();

        graphics.setColor(Color.yellow);
        sig.forEach(e -> {
            Parada centro = nodos.stream().min(Comparator.comparing(o -> dist(o, e))).get();
            graphics.drawLine(
                    centro.getCoordX().intValue(),
                    2342 - centro.getCoordY().intValue(),
                    e.getCoordX().intValue(),
                    2342 - e.getCoordY().intValue());
        });

        graphics.setColor(Color.red);
        sig.forEach(e -> drawDot(graphics, e));

        graphics.setColor(Color.GREEN);
        nodos.forEach(e -> drawDot(graphics, e));

        ImageIO.write(res, "bmp", new File(WORK_DIR + "CentrosCercanos....bmp"));
    }

    public static void mergeToImg(boolean[][] m1, boolean[][] m2) throws Exception {

        final BufferedImage res = new BufferedImage(1210, 927, BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < 927; i++) {
            for (int j = 0; j < 1210; j++) {
                try {
                    if (m1[j][i]) {
                        res.setRGB(j, i, Color.red.getRGB());
                    }
                    if (m2[j][i]) {
                        res.setRGB(j, i, Color.yellow.getRGB());
                    }
                } catch (ArrayIndexOutOfBoundsException aiobe) {
                    System.err.println("Valores: (" + j + " - " + i + ")");
                }
            }
        }
        ImageIO.write(res, "bmp", new File(WORK_DIR + "paradasM.bmp"));
    }

    public static void paradasToImg(List<String> paradas) throws Exception {

        final Double xProp = 30.8175195144;
        final Double yProp = 25.2991694915;

        final Double xShift = 554395.5453556540;
        final Double yShift = 6134610.9221502500;

        final BufferedImage res = new BufferedImage(1210, 927, BufferedImage.TYPE_INT_RGB);
        for (String elemento : paradas) {
            final String[] split = elemento.split("\t");
            final Double xImg = (Double.valueOf(split[1]) - xShift) / xProp;
            final Double yImg = (Double.valueOf(split[2]) - yShift) / yProp;
            try {
                res.setRGB(xImg.intValue(), 926 - yImg.intValue(), Color.red.getRGB());
            } catch (ArrayIndexOutOfBoundsException aiobe) {
                System.err.println("Valores: (" + xImg.intValue() + " - " + yImg.intValue() + ")");
            }
        }
        ImageIO.write(res, "bmp", new File(WORK_DIR + "paradas.bmp"));
    }

    public static void procesarMatriz() throws Exception {
        for (int i = 1; i <= 12; i++) {
            final String month = formatName(i);
            final String filePath = WORK_DIR + MATRIZ_DIR + month;

            int acumulado = 0;
            System.out.println("Procesando: " + filePath);
            for (int j = 546; j <= 6150; j++) {
                try (BufferedReader br = new BufferedReader(new FileReader(filePath + "\\" + j))) {
                    int k = 0;
                    for (String line; (line = br.readLine()) != null; ) {
                        k++;
                        acumulado += Integer.valueOf(line);
                        matriz[j - 1][k - 1] = Integer.valueOf(line);
                    }
                } catch (FileNotFoundException fnfe) {
                    System.err.println(fnfe.getMessage());
                }
            }
            System.out.println("Acumulado: " + acumulado);

            toCSV(month);
            //toBMP(month);
        }
        System.out.println("Finish");
    }

    public static void toBMP(String name) throws Exception {

        final BufferedImage res = new BufferedImage(6150, 6150, BufferedImage.TYPE_INT_RGB);
        for (int j = 0; j < 6150; j++) {
            for (int k = 0; k < 6150; k++) {
                if (matriz[j][k] == 0) {
                    res.setRGB(j, k, 0);
                    continue;
                }
                if (matriz[j][k] > 0) {
                    res.setRGB(j, k, Color.red.getRGB() - 1000 * matriz[j][k]);
                    continue;
                }
                if (matriz[j][k] > 50) {
                    res.setRGB(j, k, Color.red.getRGB());
                    continue;
                }
                if (matriz[j][k] > 25) {
                    res.setRGB(j, k, Color.orange.getRGB());
                    continue;
                }
                if (matriz[j][k] > 10) {
                    res.setRGB(j, k, Color.yellow.getRGB());
                    continue;
                }

                res.setRGB(j, k, Color.green.getRGB());
            }
        }

        ImageIO.write(res, "bmp", new File(WORK_DIR + MATRIZ_DIR + name + ".bmp"));
    }

    public static void toCSV(String name) throws Exception {
        try (PrintWriter writer = new PrintWriter(WORK_DIR + name + ".csv", "UTF-8")) {
            for (int j = 0; j < 6150; j++) {
                writer.print(matriz[j][0]);
                for (int k = 1; k < 6150; k++) {
                    writer.print("; " + matriz[j][k]);
                }
                writer.println();
            }
        }
    }

    public static void toMAT(int[][] matriz, String name) throws Exception {
        final int rows = matriz.length;
        final int cols = matriz[0].length;

        try (PrintWriter writer = new PrintWriter(WORK_DIR + name + ".mat", "UTF-8")) {
            for (int j = 0; j < rows; j++) {
                writer.print(matriz[j][0]);
                for (int k = 1; k < cols; k++) {
                    writer.print(" " + matriz[j][k]);
                }
                writer.println();
            }
        }
    }

    public static String formatName(int i) {
        return i < 10 ? "0" + i : "" + i;
    }
}
