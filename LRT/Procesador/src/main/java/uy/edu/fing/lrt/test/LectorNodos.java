package uy.edu.fing.lrt.test;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class LectorNodos {


    //private final static int SIZE_X = 1074;
    //private final static int SIZE_Y = 744;

    //private final static int SIZE_X = 1913;
    //private final static int SIZE_Y = 1453;

    private final static int SIZE_X = 3726;
    private final static int SIZE_Y = 2343;

    private final static int OFF_X = 1276;
    private final static int OFF_Y = 0;


    private final static double FAC_X = 1913D / 1210D;
    private final static double FAC_Y = 1453D / 927D;

    private final static String WORK_DIR = "C:\\Tesina\\";
    private final static String MATRIZ_DIR = "Matriz_OD\\";


    public static void main(String[] args) throws Exception {
        List<Parada> nodos = leerNodosInicialesNodos();
        nodos.forEach(e -> {
            e.setCoordX(OFF_X + e.getCoordX() * FAC_X);
            e.setCoordY(OFF_Y + e.getCoordY() * FAC_Y);
        });
        Double maxX = nodos.stream().mapToDouble(Parada::getCoordX).max().orElse(-1D);
        Double maxY = nodos.stream().mapToDouble(Parada::getCoordY).max().orElse(-1D);
        Double minX = nodos.stream().mapToDouble(Parada::getCoordX).min().orElse(-1D);
        Double minY = nodos.stream().mapToDouble(Parada::getCoordY).min().orElse(-1D);

        mergeToImg(nodos);

        System.out.println("Finish");
    }

    public static List<Parada> getNodos() throws Exception {
        List<Parada> nodos = leerNodosInicialesNodos();
        nodos.forEach(e -> {
            e.setCoordX(OFF_X + e.getCoordX() * FAC_X);
            e.setCoordY(OFF_Y + e.getCoordY() * FAC_Y);
        });
        return nodos;
    }

    private static List<Parada> leerNodosInicialesNodos() throws Exception {
        List<String> elementos = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(WORK_DIR + "datos\\nodos.txt"))) {
            return br.lines().map(Parada::new).collect(Collectors.toList());
        } catch (FileNotFoundException fnfe) {
            System.err.println(fnfe.getMessage());
        }
        return new ArrayList<>();
    }

    private static List<String> leerNodosIniciales() throws Exception {
        List<String> elementos = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(WORK_DIR + "datos\\nodos.txt"))) {
            for (String line; (line = br.readLine()) != null; ) {
                elementos.add(line);
            }
        } catch (FileNotFoundException fnfe) {
            System.err.println(fnfe.getMessage());
        }

        return elementos;
    }

    private static boolean[][] nodosToMatriz(List<String> paradas) throws Exception {

        boolean[][] matriz = new boolean[1210][927];

        final Double xShift = 0.0;
        final Double yShift = 0.0;

        //final BufferedImage res = new BufferedImage(1210, 927, BufferedImage.TYPE_INT_RGB);
        for (String elemento : paradas) {
            final String[] split = elemento.split("\t");
            final Double xImg = (Double.valueOf(split[1]) - xShift);
            final Double yImg = (Double.valueOf(split[2]) - yShift);
            try {
                matriz[xImg.intValue()][926 - yImg.intValue()] = true;
            } catch (ArrayIndexOutOfBoundsException aiobe) {
                System.err.println("Valores: (" + xImg.intValue() + " - " + yImg.intValue() + ")");
            }
        }
        return matriz;
    }

    private static void mergeToImg(List<Parada> nodos) throws Exception {

        final BufferedImage res = new BufferedImage(SIZE_X, SIZE_Y, BufferedImage.TYPE_INT_RGB);
        nodos.forEach(e -> {
            try {
                res.setRGB(e.getCoordX().intValue(), SIZE_Y - 1 - e.getCoordY().intValue(), Color.yellow.getRGB());
            } catch (ArrayIndexOutOfBoundsException aiobe) {
                System.err.println("Valores: " + e);
            }
        });
        ImageIO.write(res, "bmp", new File(WORK_DIR + "paradasNodos.bmp"));
    }

    public static List<Integer> leerNodosUsados() throws Exception {
        List<Integer> elementos = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(WORK_DIR + "Parte1\\NodosUsados.txt"))) {
            for (String line; (line = br.readLine()) != null; ) {
                elementos.add(Integer.valueOf(line));
            }
        } catch (FileNotFoundException fnfe) {
            System.err.println(fnfe.getMessage());
        }
        return elementos;
    }

    public static List<Parada> filtrarNodos() throws Exception {
        List<Parada> nodos = LectorNodos.getNodos();
        List<Integer> filtro = leerNodosUsados();
        return nodos.stream().filter(e -> filtro.contains(e.getNombre())).collect(Collectors.toList());
    }
}
