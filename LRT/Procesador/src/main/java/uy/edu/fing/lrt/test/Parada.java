package uy.edu.fing.lrt.test;

public final class Parada {

    private double coordX;
    private double coordY;
    private final int nombre;

    public Parada(String line) {
        String[] split = line.split("\t");
        //0,0648407565611663	0,0647443036353733
        //-36775,6224276760000000	-397188,1712164620000000
        this.coordX = 0.0648407565611663 * Double.valueOf(split[1]) - 36775.622427676;
        this.coordY = 0.0647443036353733 * Double.valueOf(split[2]) - 397188.171216462;
        this.nombre = Integer.valueOf(split[0]);
    }

    public Double getCoordX() {
        return coordX;
    }

    public Double getCoordY() {
        return coordY;
    }

    public void setCoordX(double coordX) {
        this.coordX = coordX;
    }

    public void setCoordY(double coordY) {
        this.coordY = coordY;
    }

    public int getNombre() {
        return nombre;
    }

    public static String calcOriginalCoords(double coordX, double coordY) {
        double x = (coordX + 36775.622427676) / 0.0648407565611663;
        double y = (coordY + 397188.171216462) / 0.0647443036353733;
        return x + "\t" + y;
    }

    @Override
    public String toString() {
        return nombre + "(" + coordX + ", " + coordY + ")";
    }
}
