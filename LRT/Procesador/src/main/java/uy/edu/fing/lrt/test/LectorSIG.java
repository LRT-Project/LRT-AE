package uy.edu.fing.lrt.test;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public final class LectorSIG {


    private final static int SIZE_X = 3726;
    private final static int SIZE_Y = 2343;

    private final static String WORK_DIR = "C:\\Tesina\\";


    public static void main(String[] args) throws Exception {
        List<Parada> nodos = leerUbicacionesProcesadasNodos();
        nodos.forEach(e -> {
            e.setCoordX((e.getCoordX() -  554395.545355653972365 ) / 10D);
            e.setCoordY((e.getCoordY() - 6134610.9221502495929599) / 10D);
        });
        double maxX = nodos.stream().mapToDouble(Parada::getCoordX).max().orElse(-1D);
        double maxY = nodos.stream().mapToDouble(Parada::getCoordY).max().orElse(-1D);
        double minX = nodos.stream().mapToDouble(Parada::getCoordX).min().orElse(-1D);
        double minY = nodos.stream().mapToDouble(Parada::getCoordY).min().orElse(-1D);
        mergeToImg(nodos);
        System.out.println("Finish");
    }

    public static List<Parada> getNodos() throws Exception {
        List<Parada> nodos = leerUbicacionesProcesadasNodos();
        nodos.forEach(e -> {
            e.setCoordX((e.getCoordX() -  554395.545355653972365 ) / 10D);
            e.setCoordY((e.getCoordY() - 6134610.9221502495929599) / 10D);
        });
        return nodos;
    }

    public static List<String> leerUbicacionesCrudas() throws Exception {
        Map<Integer, String> cache = new HashMap<>();

        try (BufferedReader br = new BufferedReader(new FileReader(WORK_DIR + "ubicacionCruda.txt"))) {
            for (String line; (line = br.readLine()) != null; ) {
                final String[] split = line.split("\t");
                cache.putIfAbsent(Integer.valueOf(split[0]), split[1] + "\t" + split[2]);
            }
        } catch (FileNotFoundException fnfe) {
            System.err.println(fnfe.getMessage());
        }

        List<Map.Entry<Integer, String>> elementos = new ArrayList<>(cache.entrySet());
        elementos.sort(Comparator.comparing(Map.Entry::getKey));
        return elementos.stream().map(v -> v.getKey() + "\t" + v.getValue()).collect(Collectors.toList());
    }


    public static void guardarUbicacionesProcesadas(List<String> ubicaciones) throws Exception {
        try (PrintWriter writer = new PrintWriter(WORK_DIR + "ubicacionProcesada.txt", "UTF-8")) {
            for (String elemento : ubicaciones) {
                writer.println(elemento.replace(',', '.'));
            }
        }
    }


    public static List<String> leerUbicacionesProcesadas() throws Exception {
        List<String> elementos = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(WORK_DIR + "datos\\ubicacionProcesada.txt"))) {
            for (String line; (line = br.readLine()) != null; ) {
                elementos.add(line);
            }
        } catch (FileNotFoundException fnfe) {
            System.err.println(fnfe.getMessage());
        }

        return elementos;
    }

    public static List<Parada> leerUbicacionesProcesadasNodos() throws Exception {

        try (BufferedReader br = new BufferedReader(new FileReader(WORK_DIR + "datos\\ubicacionProcesada.txt"))) {
            return br.lines().map(Parada::new).collect(Collectors.toList());
        } catch (FileNotFoundException fnfe) {
            System.err.println(fnfe.getMessage());
        }
        return new ArrayList<>();
    }

    private static void mergeToImg(List<Parada> nodos) throws Exception {

        final BufferedImage res = new BufferedImage(SIZE_X, SIZE_Y, BufferedImage.TYPE_INT_RGB);
        nodos.forEach(e -> {
            try {
                res.setRGB(e.getCoordX().intValue() , SIZE_Y - 1 - e.getCoordY().intValue(), Color.red.getRGB());
            } catch (ArrayIndexOutOfBoundsException aiobe) {
                System.err.println("Valores: " + e);
            }
        });
        ImageIO.write(res, "bmp", new File(WORK_DIR + "paradasSIG.bmp"));
    }
}
