# Actividad Integradora GFaller 2018

El codigo se encuentra en [LRT](/LRT) 

## Requisitos

- [JDK11](https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html)
- [GLPK](https://www.gnu.org/software/glpk/)
- [GLPK-JAVA](http://glpk-java.sourceforge.net/)
- [MVN](https://maven.apache.org/download.cgi)

## Descarga

Se contamos con git podemos clonar el repositorio.

```git clone https://gitlab.fing.edu.uy/LRT-Project/LRT-AE.git```

Si no podemos descargarlo comprimido desde [aqui](https://gitlab.fing.edu.uy/LRT-Project/LRT-AE/-/archive/master/LRT-AE-master.zip)

```https://gitlab.fing.edu.uy/LRT-Project/LRT-AE/-/archive/master/LRT-AE-master.zip```


## Compilación

Para compilar se debe ejecutar:

```mvn clean install -DargLine='-Djava.library.path=/usr/local/lib/jni:/usr/lib/jni'```

Seteando la variable `java.library.path` con la ubicacion de `glpk-java` 


## Ejecución

```java --enable-preview -jar LRTv2/target/LRTv2.jar```


## ANEXO


### Instalacion de GLPK-JAVA

- [Descargar](https://sourceforge.net/projects/glpk-java/)
- Descomprimir 
- Verificar que se tiene `JAVA_HOME` seteado
- Ejecutar `./configure`
- Luego `make` 
  - Este paso necesita SVN
  - Si no contamos con él se puede modificar el archivo `swig/Makefile.in`
  - Quitando el comando `site` de la linea `@HAVEMVN_TRUE@	$(MVN) clean package site`
- Por ultimo ejecutamos `make install`
- Deberiamos tener una carpeta en `/usr/local/lib/jni` la cual es necesaria para que la applicación corra GLPK
